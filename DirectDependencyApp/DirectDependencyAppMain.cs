namespace DirectDependencyApp;

using MyLib;
using Sys = System;

sealed class DirectDependencyAppMain
{
	public static void Main( string[] _ )
	{
		Sys.Console.WriteLine( nameof( DirectDependencyAppMain ) );
		MyType.DoYourThing();
		Sys.Console.Write( "Press [Enter] to continue: " );
		Sys.Console.ReadLine();
	}
}
