# LoadingNativeLibraries<br/><sub><sup>(Or at least trying to)</sup></sub>

This is for [Stack Overflow: "Dynamically loading _any_ assembly"](https://stackoverflow.com/q/78082305/773113)

Troubleshooting how to dynamically load an assembly which depends on an assembly which wraps a native library.

In this case, the assembly which wraps a native library (the "wrapper") is nuget package `LibGit2Sharp`

The native library is `git2-106a5f2.dll`.

MyLib - An assembly which depends on the wrapper.

DirectDependencyApp - an application that has a direct dependency on MyLib and invokes it to prove it works.

DynamicLoadingApp - an application that dynamically loads MyLib and invokes it. (Does not work.)
