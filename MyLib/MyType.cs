namespace MyLib;

using Sys = System;
using SysIo = System.IO;

public static class MyType
{
	public static void DoYourThing()
	{
		string currentDirectory = SysIo.Directory.GetCurrentDirectory();
		Sys.Console.WriteLine( $"Current Directory: {currentDirectory}" );
		string repositoryDirectory = LibGit2Sharp.Repository.Discover( currentDirectory );
		Sys.Console.WriteLine( $"Repository Directory: {repositoryDirectory}" );
		bool valid = LibGit2Sharp.Repository.IsValid( repositoryDirectory );
		Sys.Console.WriteLine( $"Is a valid repository: {valid.ToString().ToUpperInvariant()}" );
	}
}
