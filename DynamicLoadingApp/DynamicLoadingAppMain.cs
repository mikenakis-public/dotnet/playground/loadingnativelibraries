namespace DynamicLoadingApp;

using System.Linq;
using Sys = System;
using SysIo = System.IO;
using SysReflect = System.Reflection;

sealed class DynamicLoadingAppMain
{
	static void Main( string[] arguments )
	{
		Sys.Console.WriteLine( nameof( DynamicLoadingAppMain ) );
		string assemblyFilePath = SysIo.Path.GetFullPath( arguments.Single() );
		// Commented-out code makes no difference:
		// string assemblyDirectoryPath = SysIo.Path.GetDirectoryName( assemblyFilePath ) ?? throw new Sys.NullReferenceException();
		// Sys.AppDomain.CurrentDomain.AssemblyResolve += onAssemblyResolve;
		// using( Helpers.TemporarilySwitchCurrentDirectory( assemblyDirectoryPath ) )
		{
			SysReflect.Assembly assembly = SysReflect.Assembly.LoadFrom( assemblyFilePath );
			SysReflect.MethodInfo methodInfo = assembly.GetTypes().Single().GetMethod( "DoYourThing" ) ?? throw new Sys.Exception();
			methodInfo.Invoke( null, null );
		}
		Sys.Console.Write( "Press [Enter] to continue: " );
		Sys.Console.ReadLine();
		// return;
		//
		// SysReflect.Assembly? onAssemblyResolve( object? sender, Sys.ResolveEventArgs e )
		// {
		// 	Sys.Console.WriteLine( $"AssemblyResolve: {e.Name}, Requesting Assembly: {e.RequestingAssembly?.Location}" );
		// 	if( e.RequestingAssembly != null && e.RequestingAssembly.Location == assemblyFilePath )
		// 	{
		// 		Sys.Console.WriteLine( "The requesting assembly is the assembly we are loading!" );
		// 		using( Helpers.TemporarilySwitchCurrentDirectory( assemblyDirectoryPath ) )
		// 		{
		// 			SysReflect.AssemblyName assemblyName = new( e.Name );
		// 			return SysReflect.Assembly.LoadFrom( assemblyName.Name + ".dll" );
		// 		}
		// 	}
		// 	return null;
		// }
	}
}
