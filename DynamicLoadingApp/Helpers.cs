namespace DynamicLoadingApp;

using Sys = System;
using SysIo = System.IO;

static class Helpers
{
	public sealed class MakeshiftDisposable : Sys.IDisposable
	{
		readonly Sys.Action procedure;

		public MakeshiftDisposable( Sys.Action procedure )
		{
			this.procedure = procedure;
		}

		public void Dispose()
		{
			procedure.Invoke();
		}
	}

	public static Sys.IDisposable TemporarilySwitchCurrentDirectory( string newCurrentDirectory )
	{
		string oldCurrentDirectory = SysIo.Directory.GetCurrentDirectory();
		SysIo.Directory.SetCurrentDirectory( newCurrentDirectory );
		return new MakeshiftDisposable( () => SysIo.Directory.SetCurrentDirectory( oldCurrentDirectory ) );
	}
}
